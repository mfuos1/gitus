#!/bin/bash -eu

ROOT_DIR="${0%/*}/.."

cd "${ROOT_DIR}"
mkdir build
pushd build
cmake -S .. -B . -G Ninja -DCMAKE_BUILD_TYPE=Debug
cmake --build .
popd
